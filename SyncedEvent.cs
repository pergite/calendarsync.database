﻿using System;
using Starcounter;
using Simplified.Ring3;

namespace CalendarSync.Database
{
    public enum StatusEnum
    {
        Declined = -1,
        Tentative = 0,
        Confirmed = 1
    }

    [Database]
    public class SyncedEvent
    {
        public SyncedCalendar Calendar;

        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool FullDay { get; set; }

        public StatusEnum Status { get; internal set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }

        public string Organizer { get; set; }
        public string Participants { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public SystemUser Owner { get; set; }

        /* INTERNAL - DO NOT SET/UPDATE */
        public bool RemoteOrigin { get; internal set; }
        public string RemoteId { get; internal set; }
        public string iCalUID { get; internal set; }
        /* INTERNEL - END */
    }
}
