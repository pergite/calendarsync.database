﻿using System;
using Starcounter;
using Simplified.Ring3;

namespace CalendarSync.Database
{
    public enum SyncedCalendarTypeEnum
    {
        Calendar = 1,
        Resource = 2
    }

    [Database]
    public class SyncedCalendar
    {
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public string CalendarId { get; set; }
        public string Name { get; set; }
        public SyncedCalendarTypeEnum Type { get; set; }
        public DateTime? LastSync { get; set; }
        public SystemUser Owner { get; set; }

        public QueryResultRows<SyncedEvent> Events => Db.SQL<SyncedEvent>("SELECT se FROM CalendarSync.Database.SyncedEvent se WHERE se.Calendar=?", this);

        /* INTERNAL - DO NOT SET/UPDATE */
        public string SyncToken { get; internal set; }
        public string WatchId { get; internal set; }
        public string WatchRemoteId { get; internal set; }
        public DateTime WatchExpires { get; internal set; }
        /* INTERNEL - END */
    }
}
